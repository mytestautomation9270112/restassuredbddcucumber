Feature: Trigger Delete API

@Delete_API
Scenario: Delete resource
	Given The endpoint for deleting resource
	When Send the request to delete resource
	Then Validate status code to be 204