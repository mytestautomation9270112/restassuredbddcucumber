Feature: Trigger Put api with required params

@Put_API
Scenario: Trigger Put_API with valid request body params
	Given Update NAME and JOB in the request body
	When Send the request with the payload
	Then Validate the status code
	And Validate the response body params

@Put_API
Scenario Outline: Test Put API with multiple data
	Given Update "<NAME>" and "<JOB>" in the request body
	When Send the request with the payload
	Then Validate the status code
	And Validate the response body params
	
Examples: 
	|NAME|JOB|
	|ASH|DEV|
	|SAM|TL|