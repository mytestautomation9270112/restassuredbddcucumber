Feature: Trigger Patch API with the required parameters

@Patch_API
Scenario: Trigger patch_api request with valid request parameters
	Given Update NAME and JOB in request body
	When Send request with the payload
	Then Validate status code to be equal to 200
	And Validate the response parameters

@Patch_API	
Scenario Outline: Test the API with multiple data
	Given Update "<NAME>" and "<JOB>" in request body
	When Send request with the payload
	Then Validate status code to be equal to 200
	And Validate the response parameters
	
Examples:
	|NAME|JOB|
	|DAVE|PM|
	|BOB|BA|