Feature: Trigger Get api to retrieve user details

@Get_API
Scenario: Retrieve list of users
	Given Set the endpoint
	When Send the request
	Then Validate the status code to be 200
	And Validate the response body parameters