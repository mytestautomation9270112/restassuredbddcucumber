package cucumber.Options;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/Features", glue = {"stepDefinitions"}, tags = "@Post_API or @Put_API or @Patch_API or @Get_API or @Delete_API")

public class API_Test_Runner_multipleTags {

}
