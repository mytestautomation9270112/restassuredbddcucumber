package commonMethods;

import environmentAndRepository.RequestRepository;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class APITrigger extends RequestRepository{
	static String headername = "Content-Type";
	static String headervalue = "application/json";	
	public static Response Post_API_trigger(String requestBody, String endpoint) {
		RequestSpecification req_spec = RestAssured.given();
		//Set the header
		req_spec.header(headername, headervalue);
		//Set the request body
		req_spec.body(requestBody);
		//Trigger the API
		Response resp = req_spec.post(endpoint);
		return resp;		
	}
	public static Response Put_API_trigger(String requestBody, String endpoint) {
		RequestSpecification req_spec = RestAssured.given();
		//Set the header
		req_spec.header(headername, headervalue);
		//Set the request body
		req_spec.body(requestBody);
		//Trigger the API
		Response resp = req_spec.put(endpoint);
		return resp;		
	}
	public static Response Patch_API_trigger(String requestBody, String endpoint) {
		RequestSpecification req_spec = RestAssured.given();
		//Set the header
		req_spec.header(headername, headervalue);
		// Set the request body
		req_spec.body(requestBody);
		// Trigger the API
		Response resp = req_spec.patch(endpoint);
		return resp;		
	}
	public static Response Get_API_trigger(String requestBody, String endpoint) {
		RequestSpecification req_spec = RestAssured.given();
		//Trigger the API
		Response resp = req_spec.get(endpoint);
		return resp;		
	}
	public static Response Delete_API_trigger(String requestBody, String endpoint) {
		RequestSpecification req_spec = RestAssured.given();				
		//Trigger the API
		Response resp = req_spec.delete(endpoint);
		return resp;		
	}

}
