package commonMethods;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class Testng_Retry_Analyser implements IRetryAnalyzer{
	
	private int start = 0;
	private int end = 5;
	
	public boolean retry(ITestResult result) {
		if(start<end) {
			String testcase_name = result.getName();
			System.out.println(testcase_name + " failed in current iteration " + start + ", hence, retrying for iteration " + (start+1));
			start++;
			return true;
		}
		return false;
	}

}
