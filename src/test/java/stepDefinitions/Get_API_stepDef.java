package stepDefinitions;

import java.util.List;

import org.testng.Assert;

import commonMethods.APITrigger;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Get_API_stepDef {
	String requestBody;
	String endpoint;
	Response response;
	ResponseBody responseBody;
	
	@Given("Set the endpoint")
	public void set_the_url() {
		requestBody = environmentAndRepository.RequestRepository.get_requestBody();
		endpoint = environmentAndRepository.Environment.get_endpoint();
		// throw new io.cucumber.java.PendingException();
	}

	@When("Send the request")
	public void send_the_request() {
		response =APITrigger.Get_API_trigger(requestBody, endpoint);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate the status code to be 200")
	public void validate_status_code() {
		int statuscode = response.statusCode();
		Assert.assertEquals(statuscode, 200);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate the response body parameters")
	public void validate_response_body_parameters() {
		int page = 2;
		int per_page= 6;
		int total = 12;
		int total_pages = 2;
		
		int[] id = {7,8,9,10,11,12};
		String[] email = {"michael.lawson@reqres.in","lindsay.ferguson@reqres.in","tobias.funke@reqres.in","byron.fields@reqres.in","george.edwards@reqres.in","rachel.howell@reqres.in"};
		String[] first_name = {"Michael","Lindsay","Tobias","Byron","George","Rachel"};
		String[] last_name = {"Lawson","Ferguson","Funke","Fields","Edwards","Howell"};
		String[] avatar = {"https://reqres.in/img/faces/7-image.jpg","https://reqres.in/img/faces/8-image.jpg","https://reqres.in/img/faces/9-image.jpg","https://reqres.in/img/faces/10-image.jpg","https://reqres.in/img/faces/11-image.jpg","https://reqres.in/img/faces/12-image.jpg"};
		
		String support_url = "https://reqres.in/#support-heading";
		String support_text = "To keep ReqRes free, contributions towards server costs are appreciated!";
		
		responseBody = response.getBody();
		int res_page = responseBody.jsonPath().getInt("page");
		int res_per_page = responseBody.jsonPath().getInt("per_page");
		int res_total = responseBody.jsonPath().getInt("total");
		int res_total_pages = responseBody.jsonPath().getInt("total_pages");
		
		Assert.assertEquals(res_page, page, "Page in response is not equal to expected page");
		Assert.assertEquals(res_per_page, per_page, "Per page in response is not equal to expected per page");
		Assert.assertEquals(res_total, total,"Total in response is not equal to expected total");
		Assert.assertEquals(res_total_pages, total_pages,"Total pages in response is not equal to expected total pages");
		
		List<String> dataarray = responseBody.jsonPath().getList("data");
		int count = dataarray.size();
		
		for(int i = 0; i<count; i++) {
			int exp_id = id[i];
			String exp_email = email[i];
			String exp_first_name = first_name[i];
			String exp_last_name = last_name[i];
			String exp_avatar = avatar[i];
			
			
			int res_id = responseBody.jsonPath().getInt("data["+i+"].id");
			String res_email = responseBody.jsonPath().getString("data["+i+"].email");
			String res_first_name = responseBody.jsonPath().getString("data["+i+"].first_name");
			String res_last_name = responseBody.jsonPath().getString("data["+i+"].last_name");
			String res_avatar = responseBody.jsonPath().getString("data["+i+"].avatar");
			
			Assert.assertEquals(res_id, exp_id, "data id in response is not equal to expected data id");
			Assert.assertEquals(res_email, exp_email, "data email in response is not equal to expected email");
			Assert.assertEquals(res_first_name, exp_first_name, "data first name in response is not equal to expected first name");
			Assert.assertEquals(res_last_name, exp_last_name, "data last name in response is not equal to expected last name");
			Assert.assertEquals(res_avatar, exp_avatar, "data avatar in response is not equal to expected avatar");
		}
		
		Assert.assertEquals(responseBody.jsonPath().getString("support.url"), support_url, "Support url in response is not equal to expected Support url");
		Assert.assertEquals(responseBody.jsonPath().getString("support.text"), support_text, "Support text in response is not equal to expected Support text");
		
		
	}

}
