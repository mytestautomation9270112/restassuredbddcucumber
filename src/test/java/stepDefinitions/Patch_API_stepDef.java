package stepDefinitions;


import java.time.LocalDateTime;

import org.testng.Assert;

import commonMethods.APITrigger;
import environmentAndRepository.Environment;
import environmentAndRepository.RequestRepository;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Patch_API_stepDef {
	String endpoint;
	String requestBody;
	Response response;
	ResponseBody responseBody;
	
	@Given("Update {string} and {string} in request body")
	public void update_and_in_request_body(String string, String string2) {
	    requestBody = "{\r\n"
	    		+ "    \"name\": \""+string+"\",\r\n"
	    		+ "    \"job\": \""+string2+"\"\r\n"
	    		+ "}";
	    endpoint = Environment.patch_endpoint();
	    //throw new io.cucumber.java.PendingException();
	}
	
	@Given("Update NAME and JOB in request body")
	public void update_name_and_job_in_request_body() {
	    endpoint = Environment.patch_endpoint();
	    requestBody = RequestRepository.patch_requestBody();
	    //throw new io.cucumber.java.PendingException();
	}
	@When("Send request with the payload")
	public void send_request_with_the_payload() {
	   response  = APITrigger.Patch_API_trigger(requestBody, endpoint);
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("Validate status code to be equal to {int}")
	public void validate_status_code_to_be_equal_to(Integer int1) {
	    int statuscode = response.statusCode();
	    Assert.assertEquals(statuscode, int1);
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("Validate the response parameters")
	public void validate_the_response_parameters() {
	    responseBody = response.getBody();
	    String name = responseBody.jsonPath().getString("name");
	    String job = responseBody.jsonPath().getString("job");
	    String updatedAt = responseBody.jsonPath().getString("updatedAt");
	    updatedAt = updatedAt.toString().substring(0, 11);
	    
	    JsonPath jsp = new JsonPath(requestBody);
	    String exp_name = jsp.getString("name");
	    String exp_job = jsp.getString("job");
	    
	    LocalDateTime currentdate = LocalDateTime.now();
	    String exp_date = currentdate.toString().substring(0,11);
	    
	    Assert.assertEquals(name, exp_name, "Name in response is not equal to expected name");
	    Assert.assertEquals(job, exp_job, "Job in response is not equal to expected job");
	    Assert.assertEquals(updatedAt, exp_date, "Date in response is not equal to expected date");
	    
	    //throw new io.cucumber.java.PendingException();
	}


}
