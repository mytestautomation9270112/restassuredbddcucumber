package stepDefinitions;

import java.time.LocalDateTime;

import org.testng.Assert;

import commonMethods.APITrigger;
import environmentAndRepository.Environment;
import environmentAndRepository.RequestRepository;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Put_API_StepDef {
	String requestBody;
	String endpoint;
	Response response;
	ResponseBody resBody;
	
	@Given("Update {string} and {string} in the request body")
	public void update_and_in_the_request_body(String string, String string2) {
	    requestBody = "{\r\n"
	    		+ "    \"name\": \""+string+"\",\r\n"
	    		+ "    \"job\": \""+string2+"\"\r\n"
	    		+ "}";
	    endpoint = Environment.put_endpoint();
	   // throw new io.cucumber.java.PendingException();
	}
	
	@Given("Update NAME and JOB in the request body")
	public void update_name_and_job_in_the_request_body() {
	   requestBody = RequestRepository.put_requestBody();
	   endpoint = Environment.put_endpoint();
	   //throw new io.cucumber.java.PendingException();
	}
	@When("Send the request with the payload")
	public void send_the_request_with_the_payload() {
	    response = APITrigger.Put_API_trigger(requestBody, endpoint);
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("Validate the status code")
	public void validate_the_status_code() {
	    int statuscode = response.statusCode();
	    Assert.assertEquals(statuscode, 200);
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("Validate the response body params")
	public void validate_the_response_body_params() {
	    resBody = response.getBody();
	    String res_name = resBody.jsonPath().getString("name");
	    String res_job = resBody.jsonPath().getString("job");
	    String res_updatedAt = resBody.jsonPath().getString("updatedAt");
	    
	    JsonPath jsp = new JsonPath(requestBody);
	    String exp_name = jsp.getString("name");
	    String exp_job = jsp.getString("job");
	    
	    LocalDateTime currentDate = LocalDateTime.now();
	    
	    Assert.assertEquals(res_name, exp_name, "Name in response is not equal to expected name");
	    Assert.assertEquals(res_job, exp_job, "Job in response is not equal to expected job");
	    Assert.assertEquals(res_updatedAt.toString().substring(0, 11), currentDate.toString().substring(0, 11), "Date in response is not equal to expected date");
	    //throw new io.cucumber.java.PendingException();
	}

}
