package stepDefinitions;

import org.testng.Assert;

import commonMethods.APITrigger;
import environmentAndRepository.Environment;
import environmentAndRepository.RequestRepository;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Delete_API_stepDef {
	String requestBody;
	String endpoint;
	Response response;
	
	@Given("The endpoint for deleting resource")
	public void the_endpoint_for_deleting_resource() {
	    endpoint = Environment.delete_endpoint();
	    requestBody = RequestRepository.delete_requestBody();
	    //throw new io.cucumber.java.PendingException();
	}
	@When("Send the request to delete resource")
	public void send_the_request_to_delete_resource() {
	    response = APITrigger.Delete_API_trigger(requestBody, endpoint);
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("Validate status code to be {int}")
	public void validate_status_code_to_be(Integer int1) {
	    int statuscode = response.statusCode();
	    Assert.assertEquals(statuscode, int1);
	    //throw new io.cucumber.java.PendingException();
	}

}
